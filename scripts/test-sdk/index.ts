
// You can also import another NPM package 
import SDK from "@mocobaas/server-sdk";

async function handler(ctx: SDK.Context): Promise<SDK.ReturnCtx>  {
  return {
    data: ctx.data,
    error: null
  }
}

module.exports = handler;
