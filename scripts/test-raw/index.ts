
// You can also import another NPM package 
import SDK from "@mocobaas/server-sdk";

async function get(ctx: SDK.Context): Promise<SDK.ReturnCtx> {
  return {
    data: "get test-raw"
  };
}

async function post(ctx: SDK.Context): Promise<SDK.ReturnCtx>  {
  return {
    data: ctx.data
  };
}

async function put(ctx: SDK.Context): Promise<SDK.ReturnCtx>  {
  return {
    data: ctx.data
  };
}

module.exports = { get, post, put };
