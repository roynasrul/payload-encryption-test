// food seeder file
// fill empty array with object, example below

// module.exports = [{
//   id: "377e439b-33eb-4fea-b546-6399ce20bb39",
//   name: "harry potter",
//   author: "j k rowling",
// },{
//   id: "5afb470e-2a15-49a6-ac03-1ad369c19b77",
//   name: "the da vinci code",
//   author: "dan brown",
// }];

module.exports = [
  {
    id: "fe547e59-c355-47e3-a77b-613c24f96ed0",
    created_at: "2021-07-15T16:51:21.539Z",
    updated_at: "2021-07-15T16:51:21.539Z",
    name: "sate",
    price: 10000,
  },
  {
    id: "e4018abb-68d4-4404-a2cb-4edd7b8189ec",
    created_at: "2021-07-15T16:51:30.357Z",
    updated_at: "2021-07-15T16:51:30.357Z",
    name: "bakso",
    price: 5000,
  },
  {
    id: "2da71826-db6b-461d-af09-d523f7a6799e",
    created_at: "2021-07-15T16:51:40.130Z",
    updated_at: "2021-07-15T16:51:40.130Z",
    name: "nasi goreng",
    price: 10000,
  },
  {
    id: "2e17dea2-bcdf-42e5-b77e-ac603f434b2f",
    created_at: "2021-07-15T16:51:57.033Z",
    updated_at: "2021-07-15T16:51:57.033Z",
    name: "enak ya",
    price: 1000,
  },
];
